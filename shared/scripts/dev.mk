php-cs-fixer:
	@make toolkit.php-cs-fixer

rector:
	@make toolkit.run/"rector"

phpstan:
ifeq (, $(shell which docker))
	/srv/app/bin/console cache:warmup
	/tools/phpstan/vendor/bin/phpstan clear-result-cache
	/tools/phpstan/vendor/bin/phpstan analyse --memory-limit=2G -c /srv/phpstan.neon
else
	@make toolkit.phpstan
endif

v-phpstan:
	@make toolkit.phpstan
