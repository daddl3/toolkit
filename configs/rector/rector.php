<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Doctrine\Set\DoctrineSetList;
use Rector\PHPUnit\Set\PHPUnitSetList;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;
use Rector\Symfony\Set\SymfonySetList;
use Rector\Symfony\Set\TwigSetList;
use Rector\ValueObject\PhpVersion;
use Rector\PHPUnit\CodeQuality\Rector\Class_\YieldDataProviderRector;
use Rector\Carbon\Rector\FuncCall\DateFuncCallToCarbonRector;
use Rector\Carbon\Rector\FuncCall\TimeFuncCallToCarbonRector;
use Rector\Carbon\Rector\MethodCall\DateTimeMethodCallToCarbonRector;
use Rector\Carbon\Rector\New_\DateTimeInstanceToCarbonRector;

return RectorConfig::configure()
    ->withPhpVersion(PhpVersion::PHP_83)
    ->withSets([
        DoctrineSetList::DOCTRINE_CODE_QUALITY,
        DoctrineSetList::TYPED_COLLECTIONS,
        DoctrineSetList::DOCTRINE_BUNDLE_210,
        DoctrineSetList::DOCTRINE_COLLECTION_22,
        DoctrineSetList::DOCTRINE_COMMON_20,
        DoctrineSetList::DOCTRINE_DBAL_211,
        DoctrineSetList::DOCTRINE_ORM_214,
        SymfonySetList::CONFIGS,
        SymfonySetList::SYMFONY_CODE_QUALITY,
        SymfonySetList::SYMFONY_CONSTRUCTOR_INJECTION,
        SymfonySetList::SYMFONY_72,
        TwigSetList::TWIG_UNDERSCORE_TO_NAMESPACE,
        LevelSetList::UP_TO_PHP_80,
        LevelSetList::UP_TO_PHP_81,
        LevelSetList::UP_TO_PHP_82,
        LevelSetList::UP_TO_PHP_83,
        SetList::PHP_83,
        PHPUnitSetList::ANNOTATIONS_TO_ATTRIBUTES,
        PHPUnitSetList::PHPUNIT_CODE_QUALITY,
        PHPUnitSetList::PHPUNIT_110,
    ])
    ->withPaths([
        '/srv/codebase',
    ])
    ->withPreparedSets(
        deadCode           : true,
        codeQuality        : true,
        codingStyle        : true,
        typeDeclarations   : true,
        privatization      : true,
        naming             : true,
        instanceOf         : true,
        earlyReturn        : true,
        strictBooleans     : true,
        carbon             : true,
        rectorPreset       : true,
        phpunitCodeQuality : true,
        doctrineCodeQuality: true,
        symfonyCodeQuality : true,
        symfonyConfigs     : true
    )
    ->withAttributesSets(
        symfony   : true,
        doctrine  : true,
        phpunit   : true,
        sensiolabs: true
    )
    ->withComposerBased(
        twig    : true,
        doctrine: true,
        phpunit : true
    )
    ->withPhpSets(php83: true)
    ->withImportNames(
        removeUnusedImports: true
    )
    ->withPHPStanConfigs([
        '/srv/configs/phpstan/phpstan-dev.neon',
    ])
    ->withSkip([
        '/srv/codebase/bundles',
        '/srv/codebase/vendor',
        '/srv/codebase/bin',
        '/srv/codebase/var',
        '/srv/codebase/public',
        '/srv/codebase/assets',
        '/srv/codebase/config',
        '/srv/codebase/docker',
        '/srv/codebase/migrations',
        YieldDataProviderRector::class,
        DateFuncCallToCarbonRector::class,
        TimeFuncCallToCarbonRector::class,
        DateTimeMethodCallToCarbonRector::class,
        DateTimeInstanceToCarbonRector::class
    ]);
