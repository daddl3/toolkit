#!/usr/bin/make

SHELL = /bin/sh
PWD := $(shell pwd)
CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

VERSION := latest
ifneq ($(CI_COMMIT_BRANCH), main)
	VERSION = latest
endif

build.toolkit:
	docker build -t daddl3/toolkit -f ./build-images/Dockerfile_toolkit .

build.mysql:
	docker build -t daddl3/mysql -f ./build-images/Dockerfile_mysql .

push/%:
	docker push $*

push.mysql:
	docker push daddl3/mysql

push.toolkit:
	docker push daddl3/toolkit

composer/%:
	@docker run \
		-ti --rm \
		-v ${PWD}/configs/rector:/srv/codebase \
		-w /srv/codebase \
		-u ${CURRENT_UID}:${CURRENT_GID} \
		composer:2 composer $*
